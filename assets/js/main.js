//map initiation function
function initMap() {
    var uluru = {lat: 17.447635, lng: 78.497739};
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 15,
      center: uluru
    });

    var contentString = '<div id="content" style="width:200px; height:auto; overflow-wrap:break-word; padding:2%">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<div id="bodyContent" style="font-size:1em">'+
        '<p><b>Zephyr Motors Pvt. Ltd.</b>,' +
        '<br>Commercial Building, Road no. 36, Jubilee Hills,<br>Hyderabad - 500033</p><p style="text-align:center"><button class="btn">VISIT WEBSITE</button></p>'+
        '</div>'+
        '</div>';

    var infowindow = new google.maps.InfoWindow({
      content: contentString
    });
    google.maps.event.addListener(infowindow, 'domready', function() {

      // Reference to the DIV that wraps the bottom of infowindow
      var iwOuter = $('.gm-style-iw');

      var iwBackground = iwOuter.prev();
      // Removes white background DIV
    iwBackground.children(':nth-child(4)').css({'background-color' : '#000'});
    // Removes background shadow DIV
    iwBackground.children(':nth-child(2)').css({'display' : 'block'});
    //tail color
    iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(0, 0, 0) 0px 1px 6px', 'z-index' : '1', 'background-color':'#000'});

     // Reference to the div that groups the close button elements.
     var iwCloseBtn = iwOuter.next();

     // Apply the desired effect to the close button
     iwCloseBtn.css({opacity: '1', 'color':'#fff'});
     iwCloseBtn.html("<img src='https://flaticons.net/gd/makefg.php?i=icons/Mobile%20Application/Close.png&r=255&g=255&b=255' style='pointer-events: none; display: block; width: 10px; height: 10px; margin: 10px 12px;'>");
    });

    var icon = {
      url:"assets/icons/LocationPin.png", // url
      scaledSize: new google.maps.Size(30, 38), // scaled size
      origin: new google.maps.Point(0,0), // origin
      anchor: new google.maps.Point(0, 0) // anchor
    };
    

    //map marker
    var marker = new google.maps.Marker({
      position: uluru,
      map: map,
      icon: icon,
      animation: google.maps.Animation.DROP,
      title: 'Zephyr Motors Pvt. Ltd.'
    });
    marker.addListener('click', function() {
      infowindow.open(map, marker);
    });
    
}

  //smooth scrolling
document.querySelectorAll('a[href^="#"]').forEach(anchor => {
    anchor.addEventListener('click', function (e) {
        e.preventDefault();

        document.querySelector(this.getAttribute('href')).scrollIntoView({
            behavior: 'smooth'
        });
    });
});
//changing the active status on nav bar links
$(".nav-item").click(function(){
	$(".nav-item").removeClass("active");
	$(this).addClass("active");
});

//validation
$("#sendButton").click(function(){
  var contact=$("#contact-email");
  var message=$("#contact-message");
  
  if(contact.val()==""){
    contact.attr("border", "1px solid red");
  }
  if(message.val()==""){
    message.attr("border", "1px solid red");
  }
});

$(window).load(function(){
  // PAGE IS FULLY LOADED  
  // FADE OUT YOUR OVERLAYING DIV
  $('#overlay').fadeOut();
});

$(document).ready(function(){
	$('#nav-icon3').click(function(){
		$(this).toggleClass('open');
	});
});